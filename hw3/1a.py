#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Homework 3    Question 1a 1a.py

import math


def f(x):
    return math.sin(x)

def trap_int(f, xmin, xmax, nstep):
    area=(f(xmin)+f(xmax))/2.0
    h = (xmax-xmin)/nstep
    for i in range(1,nstep):
        x = xmin+i*h
        area = area + f(x)
    return area*h

exact = 1.0-math.cos(1.0)
print("\nTrapezoidal sin(x) integration with 16 points:")
area = trap_int(f, 0.0, 1.0, 16)
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))

print("\nTrapezoidal sin(x) integration with 32 points:")
area = trap_int(f, 0.0, 1.0, 32)
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))

print("\nTrapezoidal sin(x) integration with 64 points:")
area = trap_int(f, 0.0, 1.0, 64)
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))

print("Trapezoidal sin(x) integration with 128 points:")
area = trap_int(f, 0.0, 1.0, 128)
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))