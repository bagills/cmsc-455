#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Homework 3    Question 1b 1b.py

import math
import numpy

def gaulegf(a, b, n):
    x = []
    w = []
    for i in range(n+1):
        w.append(0.0)
        x.append(0.0)
    eps = 3.0E-14
    m = (n+1)/2
    xm = 0.5*(b+a)
    xl = 0.5*(b-a)
    for i in range(1, int(m+1)):
        z = math.cos(3.141592654*(i-0.25)/(n+0.5))
        while True:
            p1 = 1.0
            p2 = 0.0
            for j in range(1,n+1):
                p3 = p2
                p2 = p1
                p1 = ((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j

            pp = n*(z*p1-p2)/(z*z-1.0)
            z1 = z
            z = z1 - p1/pp
            if (abs(z-z1) <= eps):
                break

        x[i] = xm - xl*z
        x[n+1-i] = xm + xl*z
        w[i] = 2.0*xl/((1.0-z*z)*pp*pp)
        w[n+1-i] = w[i]
    return x, w

def f(p):
    return math.sin(p)

exact = 1.0-math.cos(1.0)
print("\nGauss Legendre sin(x) integration with 8 points:")
x, w = gaulegf(0.0, 1.0, 8)
area = 0.0
for i in range(1, 9):
  area += w[i]*f(x[i])
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))

print("\nGauss Legendre sin(x) integration with 16 points:")
x, w = gaulegf(0.0, 1.0, 16)
area = 0.0
for i in range(1, 17):
  area += w[i]*f(x[i])
print("area: " + str(area))
error = abs(area - exact)
print("error: " + str(error))
