#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Homework 3    Question 2  question2.py

import math
import numpy


def c1(x, y):
    sum = ((x - 2) ** 2) + ((y - 2) ** 2)
    return (sum <= 1)

def c2(x, y):
    sum = (x ** 2) + ((y - 2) ** 2)
    return (sum <= 4)

def c3(x, y):
    sum = (x ** 2) + (y ** 2)
    return (sum <= 9)

def compute(inc, xmin, xmax, ymin, ymax):
    total = 0
    for x in numpy.arange(xmin, xmax, inc):
        for y in numpy.arange(ymin, ymax, inc):
            if (c2(x, y) and c3(x, y)):
                if (not c1(x, y)):
                    total = total + 1
    return (inc ** 2) * total

print("\nCalculation with .1 grid:")
area = compute(0.1, -2.0, 2.0, 0.0, 3.0)
print("area: " + str(area))

print("\nCalculation with .01 grid:")
area = compute(0.01, -2.0, 2.0, 0.0, 3.0)
print("area: " + str(area))

print("\nCalculation with .001 grid:")
area = compute(0.001, -2.0, 2.0, 0.0, 3.0)
print("area: " + str(area))
