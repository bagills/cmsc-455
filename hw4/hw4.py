#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Homework 4    hw4.py 

import math

def q1(m):
    n = math.factorial(m) 
    print ("The number of ways can you lay out a deck of cards in a line is: ")
    print(long(n)) 

def q2(x, y):
    n = math.factorial(x) 
    m = math.factorial(y)
    o = math.factorial((x-y))
    denom = (m * o)
    total = (n/denom)
    print ("The number of " + str(y) + "  card hands from a deck of " + str(x) + "  cards is: ")
    print(long(total)) 

q1(52)
q2(52, 5)
