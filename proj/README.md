# CMSC 455 Project, find minimum


Given the equation:

z = exp(sin(50.0*x)) + sin(60.0*exp(y)) +
    sin(80.0*sin(x)) + sin(sin(70.0*y)) -
    sin(10.0*(x+y)) + (x*x+y*y)/4.0

Find the global minimum z in -1 < x < 1 , -1 < y < 1
Print x, y, z at end of file. Your z should be < -3.1
Use just normal programming, too slow to do this in multiple precision.


Then put best global minimum into multiple precision in some language.


From my  download  directory, sample code:
for C, mpf_math.h  mpf_sin_cos.c  mpf_exp.c  and supporting code 

Also, for Java, Big_math.java and test_Big_math.java.
            and test_apfloat.java

Also, for Python, test_mpmath.py  need python 3 on linux.gl


There are many local minima, do not get stuck in one of them.

A global search with dx and dy <= 0.001 should be in the
global minimum.


From the global search starting point, use optimization.


Submitting your Project
 The project source and output is to be submitted on GL as 
   
   submit cs455 proj list-of-source-files 
   
   text_output-file
   
   one file should be your text output file

 The list-of-files should include source code and output
 and other files that were used.
 Do not submit executable file(s).

 The goal of the project is to give you a useful
 numeric code that you can apply as needed to future
 tasks. This includes converting a working numeric code
 to your language of choice or being able to interface
 some working numeric code with your language of choice.

 Copying of a project results is zero points for everyone
 involved in copying. Be sure to check your own answers
 by using several languages or several implementations.
 You may want to do the global search in a language
 that is fast, submit that code, and then start the
 multiple precision at the x,y found in the global search
 with a small dx,dy.