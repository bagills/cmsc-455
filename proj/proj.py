#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Project       proj.py 

import math
import numpy
from mpmath import *

def z(x, y):
    z = (exp(sin(50.0 * x))) + (sin(60.0 * exp(y))) + (sin(80.0 * sin(x))) + (sin(sin(70.0 * y))) - (sin(10.0 * (x + y))) + (((x * x) + (y * y))/4.0)
    return z

def gen_search(x_min, x_max, y_min, y_max, inc):
    tmp = z(x_min, y_min)
    tmp_x = x_min
    tmp_y = y_min
    for x in numpy.arange(x_min, x_max, inc):
        for y in numpy.arange(y_min, y_max, inc):
            if (z(x, y) < tmp):
                tmp = z(x, y)
                tmp_x = x
                tmp_y = y
    return tmp_x, tmp_y, tmp
                
def mp_search(x_est, y_est, inc1, inc2):
    tmp = z(fsub(x_est, inc1), fsub(y_est, inc1))
    tmp_x = fsub(x_est, inc1)
    tmp_y = fsub(y_est, inc1)
    for x in numpy.arange(fsub(x_est, inc1), fadd(x_est, inc1), inc2):
        for y in numpy.arange(fsub(y_est, inc1), fadd(y_est, inc1), inc2):
            if (z(x, y) < tmp):
                tmp = z(x, y)
                tmp_x = x
                tmp_y = y
    return tmp_x, tmp_y, tmp
                
mp.dps = 200
mpf.dps = 200
inc_1e_neg200 = 1**(-200)
x_ans, y_ans, ans = gen_search(-1.0, 1.0, -1.0, 1.0, .01)
x_mp, y_mp, mp_ans = mp_search(x_ans, y_ans, .01, .001)
x_fin, y_fin, fin_ans = mp_search(x_mp, y_mp, .001, inc_1e_neg200)

print("x: " + str(x_fin))
print("y: " + str(y_fin))
print("exp(sin(50.0*x)) + sin(60.0*exp(y)) + sin(80.0*sin(x)) + sin(sin(70.0*y)) - sin(10.0*(x+y)) + (x*x+y*y)/4.0 = " + str(fin_ans))