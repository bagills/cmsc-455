#!/usr/bin/env python3
'''
Diana Bell
Homework 1
CMSC 455 Spring 2019
'''

import math

# CONSTANTS
LENGTH = 0.311          #meters
DIAMETER = 0.241        #meters
BODY_AREA = 0.000506    #meters^2
CD_OF_BODY = 0.45       #dimensionless
FINS_AREA = 0.00496     #meters^2
CD_OF_FINS = 0.01       #dimensionless
MASS = 0.0340           #kilograms  
ENGINE_INITIAL = 0.0242 #kilograms
ENGINE_FINAL = 0.0094   #kilograms

IMPULSE = 8.82          #newton seconds
PEAK_THRUST = 14.09     #newtons
AVG_THRUST = 4.74       #newtons
BURN_TIME = 1.86        #seconds

DT = 0.1                #seconds
RHO = 1.293             #kilograms/meter^3
G = 9.80665             #meters/second^2

thrust = [0,6,14.1,5.5,4.5,4.3,4.3,4.3,4.3,4.3,4.3,4.1,4.1,4.3,4.3,4.3,4.3,4.5,4.5,0]
thrust_total = 0
for i in thrust:
    thrust_total = thrust_total + i

def drag_force(Cd, A, v):
    Fd = (Cd* RHO * A * math.pow(v, 2))/2
    return Fd

def gravity_force(m):
    Fg = m * G
    return Fg

def net_force(Ft, v, m):
    Fd_body = drag_force(CD_OF_BODY, BODY_AREA, v)
    Fd_fins = drag_force(CD_OF_FINS, FINS_AREA, v)
    Fg = (m + MASS)
    F_net = Ft - (Fd_body + Fd_fins + Fg)
    return F_net

def acceleration(F, m):
    a = F/(m)
    return a

def delta_velocity(a):
    dv = a * DT
    return dv

def new_velocity(v_old, dv):
    v = v_old + dv
    return v

def delta_distance(v):
    ds = (v * DT)
    return ds

def update_distance(s_old, ds):
    s = s_old + ds
    return s

def update_mass(m_old, Ft):
    m = m_old - (0.0001644 * Ft)
    return m

def update_time(t_old):
    t = t_old + DT
    return t

def main():
    # Initial conditions
    t = 0                       #time (seconds)
    s = 0                       #height (meters)
    v = 0                       #velocity (meters/second)
    a = 0                       #acceleration (meters/second^2)
    F = 0                       #total force not including gravity (newtons)
    Ft = 0                      #thrust, initially 0 (newtons)
    m = ENGINE_INITIAL   #mass (kilograms)
    Ft_index = 0
    while (v >= 0):

        
        print("\n" + str(t)+ ", " + str(s) + ", " + str(v) + ", " + str(a) + ", " + str(m + MASS))
        
        t = update_time(t)
        
        if (Ft_index < len(thrust)-1):
            Ft_index = Ft_index + 1
            Ft = thrust[Ft_index]
        else:
            Ft = 0
        
        if (m > ENGINE_FINAL):
            m = update_mass(m, Ft)
    
        F = net_force(Ft, v, m)
        a = acceleration(F, m)
        dv = delta_velocity(a)
        v = new_velocity(v, dv)
        ds = delta_distance(v)
        s = update_distance(s, ds)
        
    #TODO: add comments

if __name__ == "__main__":
    main()
 