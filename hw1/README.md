# Rocket Science

Some physical problems are easy to solve numerically using just
the basic equations of physics. Other problems may be very difficult.

Consider a specific model rocket with a specific engine.
Given all the data we can find, compute the maximum altitude
the rocket can obtain. Yes, this is rocket science.



**Estes Alpha III**

Length      12.25  inches =  0.311 meters
Diameter     0.95  inches =  0.0241 meters
Body area    0.785 square inches = 0.506E-3  square meters cross section
Cd of body   0.45  dimensionless
Fins area    7.69  square inches = 0.00496  square meters total for 3 fins
Cd of fins   0.01  dimensionless
Weight/mass  1.2   ounce  = 0.0340 kilogram without engine
Engine       0.85  ounce  = 0.0242 kilogram initial engine mass
Engine       0.33  ounce  = 0.0094 kilogram final engine mass



**Thrust curve**

Total impulse  8.82 newton seconds (area under curve)
Peak thrust   14.09 newton
Average thrust 4.74 newton
Burn time      1.86 second


**Initial conditions:**

  t = 0   time
  
  s = 0   height
  
  v = 0   velocity
  
  a = 0   acceleration
  
  F = 0   total force not including gravity
  
  m = 0.0340 + 0.0242  mass
  


**Basic physics:**

  Fd = Cd*Rho*A*v^2 /2   two equations, body and fins 
            Fd is force of drag in newtons in opposite direction of velocity
            Cd is coefficient of drag, dimensionless (depends on shape)
            Rho is density of air, use 1.293 kilograms per meter cubed
            A is total surface area in square meters
            v is velocity in meters per second (v^2 is velocity squared)

  Fg = m*g  Fg is force of gravity toward center of Earth
            m is mass in kilograms
            g is acceleration due to gravity, 9.80665 meters per second squared

  Ft =     value from thrust curve at this time, you enter this data.
           Do not copy! This is part of modeling and simulation.
           start with first non zero thrust.

  F = Ft - (Fd body + Fd fins + Fg)   resolve forces

  a = F/m   a is acceleration we will compute from knowing
            F, total force in newtons and
            m is mass in kilograms of body plus engine mass that changes

  dv = a*dt dv is velocity change in meters per second in time dt
            a is acceleration in meters per second squared
            dt is delta time in seconds

  v = v+dv v is new velocity after the dt time step
            (v is positive upward, stop when v goes negative)
            v+ is previous velocity prior to the dt time step
            dv is velocity change in meters per second in time dt 

  ds = v*dt ds is distance in meters moved in time dt
            v is velocity in meters per second
            dt is delta time in seconds

  s = s+ds s is new position after the dt time step
            s+ is previous position prior to the dt time step
            ds is distance in meters moved in time dt 

  m = m -0.0001644*Ft  apply each time step

  t = t + dt  time advances

  print t, s, v, a, m

  if v < 0  quit, else loop
            Ft is zero at and beyond 1.9 seconds, rocket speed decreases


**Homework Problem 1:**

Write a small program to compute the maximum height when
the rocket is fired straight up. Assume no wind.
In order to get reasonable consistency of answers, use dt = 0.1 second
Every student will have a different answer.
Some where near 350 meters that is printed on the box. +/- 30%
Any two answers that are the same, get a zero.

Suggestion: Check the values you get from the thrust curve by
simple summation. Using zero thrust at t=0 and t=1.9 seconds, sampling
at 0.1 second intervals, you should get a sum of about 90 . Adjust
values to make it this value in order to get reasonable consistency of
answers.

The mass changes as the engine burns fuel and expels mass
at high velocity. Assume the engine mass decreases from 0.0242 kilograms
to 0.0094 grams proportional to thrust. Thus the engine mass is
decreased each 0.1 second by the thrust value at that time times 
(0.0242-0.0094)/90.0 = 0.0001644 . mass=mass-0.0001644*thrust at this time.
"thrust" = 0.0 at time t=0.0 seconds
"thrust" = 6.0 at time t=0.1 seconds.
"thrust" = 0.0 at and after 1.9 seconds. Important, rocket is still climbing.
Check that the mass is correct at the end of the flight. 0.0340+0.0094

Published data estimates a height of 1100 feet, 335  meters to
1150 feet, 350 meters.
Your height will vary.

Your homework is to write a program that prints every 0.1 seconds:
the time in seconds
height in meters
velocity in meters per second
acceleration in meters per second squared
force in newtons
mass in kilograms (just numbers, all on one line)
and stop when the maximum height is reached.

Think about what you know. It should become clear that at each
time step you compute the body mass + engine mass, the three
forces combined into Ft-Fd_body-Fd_fins-Fg, the acceleration, the velocity
and finally the height. Obviously stop without printing if
the velocity goes negative (the rocket is coming down).

The program has performed numerical double integration. You might
ask "How accurate is the computation?"
Well, the data in the problem statement is plus or minus 5%.
We will see later that the computation contributed less error.
A small breeze would deflect the rocket from vertical and
easily cause a 30% error. We should say that:
"the program computed the approximate maximum height."
