# Least Square Fit

In a language of your choice, write a program that
does a least square fit of the thrust data used in Homework 1.

Use your data  x =  0.0  0.1   0.2  ...  1.9
               
               y =  0.0  6.0  14.1  ...  0.0

Be sure time 0.0 has value 0.0, time 1.9 has value 0.0

Do the least square fit with 3, 4, ... , 17 degree polynomials.
Compute the maximum error, the average error and RMS error
for each fit. If convenient, look at the plots of your fit
and compare to the input.

Print out your data. Remove excess printout, print:

Your polynomial order for each degree 3..17 and
Maximum, average and RMS error for each.


**The steps are typically:**

Have the thrust data in X array, time values.

Have the thrust data in Y array, newton values.

Build the least square simultaneous equations, n=3 first

Given a value of Y for each value of X, 
  
  Y_approximate =  C0 + C1 * X + C2 * X^2 +  C3 * X^3


**Then the linear equations would be:**

| SUM(1  *1) SUM(1  *X) SUM(1  *X^2) SUM(1  *X^3) | | C0 |   | SUM(1  *Y) |

| SUM(X  *1) SUM(X  *X) SUM(X  *X^2) SUM(X  *X^3) | | C1 |   | SUM(X  *Y) |

| SUM(X^2 *1) SUM(X^2 *X) SUM(X^2 *X^2) SUM(X^2 *X^3) |x| C2 | = | SUM(X^2 *Y) |

| SUM(X^3 *1) SUM(X^3 *X) SUM(X^3 *X^2) SUM(X^3 *X^3) | | C3 |   | SUM(X^3 *Y) |

Note that index [i][j] of the matrix has SUM(X^(i+j))

Solve the simultaneous equations for C0, C1, C2, C3
using some version of "simeq".
This is equivalent to polyfit.

Compute errors by using "peval" with the coefficients
for each X and subtract the corresponding Y.
Save maximum error, compute average and RMS error.
Print.

Repeat, advancing n to 4, 5, ..., 17

