#!/usr/bin/env python3
# Diana Bell    CMSC 455    Spring 2019
# Homework 2    lsfit.py  

import math
from numpy import array
from numpy.linalg import solve

xd=array([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9])
yd=array([0,6,14.1,5.5,4.5,4.3,4.3,4.3,4.3,4.3,4.3,4.1,4.1,4.3,4.3,4.3,4.3,4.5,4.5,0])
  
n = 3
m = len(xd)

while(n <= 17):
    print("\nDegree: " + str(n) + "\n================================================================")
    
    A = array([[0.0 for j in range(n)] for i in range(n)])
    X = array([0.0 for i in range(n)])
    Y = array([0.0 for i in range(n)])
    pwr = array([0.0 for i in range(n)])
    
    for i in range(n):
        for j in range(i):
            A[i][j] = 0.0
        Y[i] = 0.0
    for k in range(m):
        y = yd[k]
        x = xd[k]
        pwr[0] = 1.0
        for i in range(1, n):
            pwr[i] = pwr[i-1] * x
        for i in range(n):
            for j in range(n):
                A[i][j] = A[i][j] + pwr[i] * pwr[j]
            Y[i] = Y[i] + y * pwr[i]

    X = solve(A, Y)
    
    max_error = 0.0
    sum_of_abs_errors = 0.0
    for i in range(m):
        approx_y = 0.0
        for k in range(n):
            approx_y = approx_y + (X[k] * (xd[i]**k))
        y_actual = xd[i]
        if abs(y_actual - approx_y) > max_error:
            max_error = abs(y_actual - approx_y)
        sum_of_abs_errors = sum_of_abs_errors + abs(y_actual - approx_y)
    rms_error = math.sqrt((sum_of_abs_errors**2)/m)
    avg_error = (sum_of_abs_errors)/m
    
    print("Polynomial Order:\n" + str(X))
    print("RMS Error: " + str(rms_error))
    print("Max Error: " + str(max_error))
    print("Average Error: " + str(avg_error))
    
    n = n + 1 #next power
